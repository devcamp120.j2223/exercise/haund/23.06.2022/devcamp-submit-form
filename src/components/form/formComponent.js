import { Component } from "react";

class FormComponent extends Component{
    render(){
        return(
            <div className='jumbotron w-75 p-5 ml-5'>
            <div className='row mt-3'>
              <div className='col col-sm-4'>
                    <label className='font-weight-bold'>Firstname</label>
              </div>
              <div className='col col-sm-8'>
                    <input type="text" className="form-control" />
                </div>
            </div>

            <div className='row mt-3'>
              <div className='col col-sm-4'>
                    <label className='font-weight-bold'>Lastname</label>
              </div>
              <div className='col col-sm-8'>
                    <input type="text" className="form-control" />
                </div>
            </div>

            <div className='row mt-3'>
              <div className='col col-sm-4'>
                    <label className='font-weight-bold'>Country</label>
              </div>
              <div className='col col-sm-8'>
                    <select className='form-control'>
                      <option>----Chọn Quốc Gia-----</option>
                      <option>Australia</option>
                      <option>Vietnam</option>
                      <option>USA</option>
                    </select>
                </div>
            </div>

            <div className='row mt-3'>
              <div className='col col-sm-4'>
                    <label className='font-weight-bold'>Subject</label>
              </div>
              <div className='col col-sm-8'>
                    <textarea className='form-control' rows="7"></textarea>
                </div>
            </div>

            <div className='row mt-3 justify-content-end'>
              <button className='btn btn-success btn-send' >Send Data</button>
            </div>

        </div>
        )
    }
}

export default FormComponent;