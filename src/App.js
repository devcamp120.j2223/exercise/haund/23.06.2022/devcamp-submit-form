import { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

import TittleComponent from './components/title/tittleComponent';
import FormComponent from './components/form/formComponent';



class App extends Component{
  render(){
    return (
      <div className='container'>
        
          <TittleComponent/>
          <FormComponent/>
           
      </div>
    );
  }
}
  


export default App;
